ls = ['red','green']
print(ls)

# add element in a list
ls.append(1)
ls.append('abc')
ls.append([0,1,2])
ls.append(True)

#remove element from list
#ls.pop()

#delete a list
#del ls
#del ls[1]

#add element at specific location
#ls.insert(1,'yellow')

#Remove particular element
#ls.remove('abc')


#find Elements
i = ls.index('abc')
print('index of {} is {}'.format('abc',i))
print(ls)

# ls.clear()
cp = ls.copy()
print('main=',ls)
print('copy=',cp)