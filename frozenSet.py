##normal Set
ab = set()
ab.add(10)
print(ab)

##frozen set
cd = frozenset()
#cd.add(10)
print(cd)

st = frozenset({10,20,30})
#st.add(40)
for i in st:
    print(i)
print(st)