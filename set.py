#s = {10,20,30,40}
#s.add(100)
#print(type(s))
#print(s)
num = set()
num.add(40)
num.add(1)
num.add(100)
num.add(10.5)
num.add(100)
#Remove random element
num.pop()
print(num)

##Type casting in a set
st = 'Hello There'
st = list(st)
arr = [1,2,3,4,5,6,7,8,12,1,3,4]
convert = set(arr)
print(arr)
print(convert)
