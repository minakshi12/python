#ls=[]
#print(type(ls))

colors = ['red','green','magenta','orange','blue']
length = len(colors)
print('There are total {} elements in list'.format(length))

c = colors[0]
print(c.upper())

#-----update Elements#-----

#colors[1]='black'
#colors[0]=123
#colors[12]=[1,2,4,6]
#print(colors)


#slicing
sl = colors[2:]
sl = colors[-1:]
sl = colors[:2]
sl = colors[:-1]

sl = colors[::2]
sl = colors[::3]
print(sl)