a = (1,2,3,4,'python')
b = 100,200
## Indexing and slicing
print(a[1:])
print(a[:-1])
print(a[::2])

##length of Tuple
print(len(a))

##Concatenate
c = a+b
print(c)

##Iteration
d = b*3
print(d)
##Membership
print(201 in a)
print(201 not in a)

s = a.index(3)
print('index of 3 in tuple a is {}'.format(s))

##count Element
total = a.count(3)
print(total)